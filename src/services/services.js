import config from '@/config/config'
import axios from 'axios'

const authString = btoa(`${config.appKey}:${config.appSecret}`)

const loginUser = user => {
    this.$store.commit('setUser', res.data.user)
    
    localStorage.setItem('username', user.username)
    localStorage.setItem('authtoken', user.authtoken)

    return user
}

// export const authService = {
//     data() {
//         return {
//             authtoken: localStorage.getItem('authtoken')
//         }
//     },
//     computed: {
//         isAuthenticated() {
//             return this.authtoken !== null
//         }   
//     },
//     created() {
//         this.$root.$on('logged-in', authtoken => this.authtoken = authtoken)
//     }
// }

export const authenticate = {
    methods: {
        register(username, password) {
            return this.authenticate(`/user/${config.appKey}`, username, password)
        },
        login(username, password) {
            return this.authenticate(`/user/${config.appKey}/login`, username, password)
        },
        authenticate(url, username, password) {
            return axios
                .post(`https://baas.kinvey.com${url}`, {
                    username: username,
                    password: password
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Basic ${authString}`,
                    }
                })
                .then(({data}) => loginUser({
                    username: data.username,
                    authtoken: data._kmd.authtoken
                }))
        }
    },
    created() {
        // this.$http.defaults.headers.post['Authorization'] = `Basic ${authString}`
    }
}