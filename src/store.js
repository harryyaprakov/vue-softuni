import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    counter: 0,
    user: null
  },
  mutations: {
    increment (state) {
        state.counter++
    },
    setUser (state, payload) {
        state.user = payload
    },
    unsetUser (state) {
        state.user = null
    },
    getUserFromSession (state) {
        // state.user = window.$nuxt.$cookies.get('user')
        state.user = localStorage.getItem('authtoken')
    }
  },
  actions: {

  }
})
